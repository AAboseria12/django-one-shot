from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm


# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = TodoItem.objects.filter(list=todo_list)
    context = {
        "todo_list": todo_list,
        "todo_items": todo_items,
    }
    return render(request, "todos/todo_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()

    context = {
        "todo_form": form,
    }
    return render(request, "todos/todo_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect(todo_list_detail)
    else:
        form = TodoListForm(instance=todo_list)
    context = {"form": form}
    return render(request, "todos/todo_update.html", context)
